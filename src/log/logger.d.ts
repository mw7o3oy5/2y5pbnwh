export function text(str: string | object): void;
export function info(str: string | object): void;
export function error(str: string | object): void;