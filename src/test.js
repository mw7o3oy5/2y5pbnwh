const db = require('./db/connect')

;(async () => {
  console.time(1)
  await Promise.all(Array.from({ length: 10000 }).map(() => db.query('insert into test (status) values (1)')))
  console.timeEnd(1)
})()
